
"""Class for any disease"""

class Disease:
    def __init__(self, name=None, desc=None):
        self.name = name
        self.desc = desc
        self.symptoms = []
        self.reasponse = []
    def __str__(self):
        res = '\nХвороба %s:\n' % (self.name)
        for attr in self.__dict__:
            if attr == 'name': pass
            else: res += '\t%s: %s\n' % (attr, self.__dict__[attr])
        return res
    def __repr__(self):
        return self.name

class Symptom(Disease):
    def __init__(self, name='', desc='', value=[]):
        Disease.__init__(self, name=name, desc=desc)
        self.value = value
        self.type = type(value)
    def __repr__(self):
        return "\n\t\tSymptom(%s): %s\n" % (self.name, self.value)
