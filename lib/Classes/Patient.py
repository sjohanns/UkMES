
"""
Major Patient clss
"""

class Patient:
    def __init__(self, name='Person', characteristics={}):
        self.name = name
        self.characteristics = characteristics
        self.symptoms = []
    def __repr__(self):
        return self.name
    def __str__(self):
        res = "Пацієнт %s:\n" % (self.name)
        for attr in self.__dict__:
            if attr != 'name':
                res += "\t%s: %s\n" % (attr, self.__dict__[attr])
        return res
