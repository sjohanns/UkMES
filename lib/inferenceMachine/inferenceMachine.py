
"""
Majors tree-based inference machine

Time complexy: O(|Diseases|+|Symptoms|)

"""

from lib.Classes.Disease import Disease

def inferenceEngine(diseases, patient):
    for disease in diseases:
        count = 0
        for s in patient.symptoms:
            #        print('e')
            if disease.name == s.name:
                count += 100
                break
            else:
                count += searchSymp(disease, s)
        yield ("%s: %s" % (disease.name, count)) + "%"
memo = {}

def searchSymp(disease, symptom):
    count = 0
    if disease.name in memo.keys():
        return 0
    if disease.name == symptom.name:
        count = 10
        memo[symptom.name] = count
        return count
    else:
        for s in disease.symptoms:
            if isinstance(s.value, Disease):
                if s.name == symptom.name:
                    count += ((max(True, symptom.value) - min(False, symptom.value)) / symptom.value) * 10
                else:
                    count += searchSymp(s.value, symptom)
            else:
                if isinstance(s.value, list): continue
                if s.name == symptom.name:
                    count += ((max(True, symptom.value) - min(False, symptom.value)) / symptom.value) * 10
            memo[symptom.name] = count
    return count
