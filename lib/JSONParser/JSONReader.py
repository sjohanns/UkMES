"""
It's is module with functions that need be using for JSON reading file of the diseases.
"""

import json
from lib.Classes.Disease import Disease, Symptom
from lib.Classes.Patient import Patient
"""
Functions for reading disease file
"""

def addTreeFromJson(file):
    """(/typeFile/ means file of disease or patient)"""
    with open(file) as f:
        data = json.load(f)
    for dis in data:
        if '_' in dis:
            del dis['_']
        tree = Disease()
        readJson(dis, tree)
        yield tree

def readJson(data, tree):
    if not data: return "Error"
    for attr in data:
        # print(attr)
        if not isinstance(data[attr], list):
            #print(attr)
            setattr(tree, attr, data[attr])
        else:
            if attr == 'symptoms':
                symptomsReader(tree, data)
            elif attr == 'reasponse':
                setattr(tree, 'reasponse', data['reasponse'])

def symptomsReader(tree, data):
    for symptom in data['symptoms']:
        cur = Symptom()
        disease = Disease()
        if isinstance(symptom['value'], dict):
            readJson(symptom['value'], disease)
            cur.name = disease.name
            cur.value = disease
            tree.symptoms.append(cur)

        else:
            cur.name = symptom['name']
            cur.value = symptom['value']
            tree.symptoms.append(cur)


"""
Functions for reading patient file
"""

def addPatientFromJson(file):
    with open(file) as f:
        patients = json.load(f)
    for p in patients:
        patient = Patient()
        readPatient(p, patient)
        yield patient


def readPatient(data, patient):
    for attr in data.keys():
        if attr == 'symptoms':
            for s in data[attr]:
                symptom = Symptom()
                symptom.name = s['name']
                symptom.value = s['value']
                patient.symptoms.append(symptom)
        else:
            setattr(patient, attr, data[attr])
