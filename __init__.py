

"""
Test !
"""


import lib.Classes.Patient
from lib.JSONParser.JSONReader import addTreeFromJson, addPatientFromJson
from lib.inferenceMachine.inferenceMachine import inferenceEngine

diseases = addTreeFromJson('data/polyuria2.json')
patient = addPatientFromJson('data/abdultsWithPolyuria.json')

for diagn in inferenceEngine(diseases, next(patient)):
    print(diagn)
