
from hashlib import sha1
from flask import Flask, render_template, request

app = Flask(__name__, static_folder='./static')

"""
 WSGI — Web Server Gateway Interface
"""

"""
MAYBE config file
"""

class BaseConfig(object):
    SECRET_KEY = sha1(b"johann").hexdigest()
    DEBUG = True
    TESTING = False

app.config.from_object(BaseConfig) # export FLASK_DEBGU = 1

"Init app"

@app.route("/")
@app.route("/main")
def hello_world():
    user = request.args.get('user', 'Johann')
    return render_template('index.html', user=user)
if __name__ == '__main__':
    app.run()
