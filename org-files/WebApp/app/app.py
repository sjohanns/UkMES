
from hashlib import sha1
from flask import Flask

app = Flask(__name__, static_folder='./static')

"""
 WSGI — Web Server Gateway Interface
"""

"""
MAYBE config file
"""

class BaseConfig(object):
    SECRET_KEY = sha1(b"johann").hexdigest()
    DEBUG = True
    TESTING = False

app.config.from_object(BaseConfig) # export FLASK_DEBGU = 1

"Init app"

@app.route("/")
def hello_world():

if __name__ == '__main__':
    app.run()
