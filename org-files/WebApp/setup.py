#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import os
from setuptools import setup

setup(
    name='AppWebForEMS',
    version='0.0.1',
    author='Tovstolyak Johann',
    author_email='tovstolaki62@gmail.com',
    description='UI App for Expert Medical System',
    packages=['app'],
    platforms=['linux'],
    install_requires=[
        'Flask'
    ],
    classifiers=[
        'Development Status :: 4 - Beta' # https:// pypi.python.org/pypi?%3Aaction=list_classifiers
    ]
)
